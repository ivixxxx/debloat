#!/bin/bash

yay # updates system & aur packages

sudo pacman -Rns $(pacman -Qdtq) # kills all orphaned packages

sudo pacman -Sc # clears pacman cache

yay -Sc # clears yay cache